#!/bin/bash

set -euo pipefail

ECK_OPERATOR_CHART_VERSION="2.9.0"
ECK_OPERATOR_NAMESPACE="elastic-system"
LOGGING_NAMESPACE="logging"
FLUENTD_CHART_VERSION="5.8.7"

# ------------------------------------------------
helm repo add "elastic" "https://helm.elastic.co"
helm repo add "bitnami" "https://charts.bitnami.com/bitnami"
helm repo update

# ------------------------------------------------
# Install Elastic Operator
kubectl create ns "${ECK_OPERATOR_NAMESPACE}" --dry-run=client -o yaml | kubectl apply -f -
helm upgrade --install --atomic --debug "elastic-operator" "elastic/eck-operator" \
  --version "${ECK_OPERATOR_CHART_VERSION}" \
  --namespace "${ECK_OPERATOR_NAMESPACE}" \
  --timeout 300s \
  --values "elastic/values-operator.yaml"

# Check status
kubectl -n "${ECK_OPERATOR_NAMESPACE}" rollout status statefulset elastic-operator

# ------------------------------------------------
# Install Elasticsearch and Kibana
kubectl create ns "${LOGGING_NAMESPACE}" --dry-run=client -o yaml | kubectl apply -f -
kubectl apply -f "elastic/manifest-elasticsearch.yaml"
kubectl apply -f "elastic/manifest-kibana.yaml"
kubectl apply -f "elastic/manifest-ingress.yaml"

# Check status
kubectl -n "${LOGGING_NAMESPACE}" rollout status deployment kibana-kb

# ------------------------------------------------
# Install Fluentd (Bitnami)
kubectl apply -f "fluentd-bitnami/manifest-pvc.yaml"
kubectl apply -f "fluentd-bitnami/manifest-forwarder-config.yaml"
helm upgrade --install --atomic --debug "fluentd" "bitnami/fluentd" \
  --version "${FLUENTD_CHART_VERSION}" \
  --namespace "${LOGGING_NAMESPACE}" \
  --timeout 300s \
  --values "fluentd-bitnami/values-fluentd.yaml"

# Check status
kubectl -n "${LOGGING_NAMESPACE}" rollout restart daemonset fluentd
kubectl -n "${LOGGING_NAMESPACE}" rollout status daemonset fluentd
# kubectl -n "${LOGGING_NAMESPACE}" rollout status statefulset fluentd

# ------------------------------------------------
# Install Cerebro
kubectl apply -f "cerebro/manifest-deployment.yaml"
kubectl apply -f "cerebro/manifest-service.yaml"
kubectl apply -f "cerebro/manifest-ingress.yaml"