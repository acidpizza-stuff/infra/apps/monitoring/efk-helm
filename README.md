# EFK

[[_TOC_]]

## Update ECK Operator

Reference: 
- [Installation Guide](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-stack-helm-chart.html)
- [Github Repo](https://github.com/elastic/cloud-on-k8s/tree/main/deploy)
- [Support Matrix](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-supported.html)
- [Release Notes](https://github.com/elastic/cloud-on-k8s/releases)

```bash
# Get latest versions
helm repo add "elastic" "https://helm.elastic.co"
helm repo update
helm search repo elastic/eck-operator -l
```

1. Update the version number in `deployment/<cluster>/eck-operator/kustomization.yaml`.


## Update ECK Elasticsearch and Kibana

ECK Stack helm chart is only available for enterprise license. Need to install CRs manually.

Reference:

- [Elasticsearch ECK Config](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-elasticsearch-specification.html)
- [Kibana ECK Config](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-kibana.html)
- [Elasticsearch Upgrade Guide](https://www.elastic.co/guide/en/elasticsearch/reference/current/setup-upgrade.html)
- [Elasticsearch Release Notes](https://www.elastic.co/guide/en/elasticsearch/reference/current/es-release-notes.html)

1. Update the version number in `deployment/<cluster>/cluster-logging/elastic/elasticsearch-patch.yaml` and `deployment/<cluster>/cluster-logging/elastic/kibana-patch.yaml`. The Elasticsearch and Kibana version should be the same.


## Update Fluentd

### Deployment Architecture

Reference: https://fluentbit.io/blog/2020/12/03/common-architecture-patterns-with-fluentd-and-fluent-bit/

- **Forwarder:** Daemonset
- **Aggregator:** Statefulset

### Bitnami Fluentd

Reference:
- [Documentation](https://docs.bitnami.com/kubernetes/apps/fluentd/get-started/install/)
- [Github Repo](https://github.com/bitnami/charts/tree/main/bitnami/fluentd)
- [Check versions](https://artifacthub.io/packages/helm/bitnami/fluentd)
- [Upgrade Notes](https://github.com/bitnami/charts/tree/main/bitnami/fluentd#upgrading)
- [Plugins](https://www.fluentd.org/plugins)

```bash
# Get latest versions
helm repo add "bitnami" "https://charts.bitnami.com/bitnami"
helm repo update
helm search repo bitnami/fluentd -l
```

1. Update the version number in `deployment/<cluster>/cluster-logging/fluentd/kustomization.yaml`.

### Elasticsearch Plugin

Reference: https://github.com/uken/fluent-plugin-elasticsearch

Provide elasticsearch credentials via a secret.

```bash
ES_USER="elastic"
read -s "ES_PASSWORD"
kubectl -n logging create secret generic fluentd-forwarder-env-secret --from-literal=ES_USER="${ES_USER}" --from-literal=ES_PASSWORD="${ES_PASSWORD}"
```

### Kibana Configuration

Edit the index template of the data stream.

```json
{
  "index": {
    "lifecycle": {
      "name": "k8s_policy"
    },
    "number_of_replicas": "0"
  }
}
```

Also update the replicas for the first hidden index of the data stream.

```bash
curl -k -u elastic -p -XPUT https://localhost:9200/_settings -d '{"index.number_of_replicas": 0}' -H 'Content-Type: application/json'
```


## Update Cerebro

Reference:
- [Github Repo](https://github.com/lmenezes/cerebro)

Seems like the project will not be updated frequently. Consider migrating to [ElasticVue](https://github.com/cars10/elasticvue).


## Update Elasticvue

Reference:
- [Github Repo](https://github.com/cars10/elasticvue)
- [Docker Repo](https://hub.docker.com/r/cars10/elasticvue)

Update secret:

```bash
ES_USER=elastic
read -s ES_PASSWORD

kubectl -n logging create secret generic elasticvue-clusters --from-literal=config="[{\"name\": \"default cluster\", \"uri\": \"https://elasticsearch.192.168.1.132.nip.io\", \"username\": \"${ES_USER}\", \"password\": \"${ES_PASSWORD}\"}]"
```

## Troubleshooting

### ES

#### Cluster health is red due to unavailable primary and replica shards

Yellow indices indicate that shards are not replicated. Red indices indicate that primary shards are not allocated to a node.

Most likely cause is out of space. Deleting red indices is the fastest way to fix red cluster status.

```bash
# show red cluster health
kubectl -n logging get elasticsearch

# Enter elasticsearch container to access API
kubectl -n logging exec -it elasticsearch-es-default-0 -- /bin/bash

# Query cluster health report -> cluster red due to unavailable primary and replica shards
curl -k -u elastic -p https://localhost:9200/_health_report

# View shard allocation
curl -k -u elastic -p https://localhost:9200/_cat/allocation?v

# Get explanation for failed allocations
curl -k -u elastic -p https://localhost:9200/_cluster/allocation/explain?pretty

# View indices health -> find the red indices
curl -k -u elastic -p https://localhost:9200/_cat/indices?v

# Delete red indices
curl -k -XDELETE -u elastic -p https://localhost:9200/<indice-name>

# Check settings -> Check replication shards (should be 0)
curl -k -u elastic -p https://localhost:9200/_settings

# Set replication shards to 0
curl -k -u elastic -p -XPUT https://localhost:9200/_settings -d '{"index.number_of_replicas": 0}' -H 'Content-Type: application/json'

# Check unassigned shards -> find associated unassigned index
curl -k -u elastic -p 'https://localhost:9200/_cat/shards?v=true&h=index,shard,prirep,state,node,unassigned.reason&s=state'

# Get explanation
curl -k -u elastic -p "https://localhost:9200/_cluster/allocation/explain?pretty" -H 'Content-Type: application/json' -d'
{
  "index": "<index_name>", 
  "shard": 0, 
  "primary": true 
}
'

# Try to force assign a shard
curl -k -u elastic -p -XPOST https://localhost:9200/_cluster/reroute?pretty -H 'Content-Type: application/json' -d '{ "commands" : [ {
"allocate" : {
  "index" : "<index_name>", 
  "shard" : 0, 
  "node" : "<node_name>",
  "allow_primary":true 
} 
} ] }'

# Create elevated user's rights to delete kibana_task_manager index (will lose all kibana data)
curl -k -u elastic -p -XPUT https://localhost:9200/_security/role/grant_kibana_system_indices -H 'Content-Type: application/json' -d '{
  "indices": [
    {
      "names": [
        ".kibana*"
      ],
      "privileges": [
        "all"
      ],
      "allow_restricted_indices": true
     }
  ]
}'
curl -k -u elastic -p -XPOST https://localhost:9200/_security/user/temporarykibanasuperuser -H 'Content-Type: application/json' -d '{
  "password": "mypassword",
  "roles" : [ "superuser", "grant_kibana_system_indices" ]
}'
```

### fluentd

#### Cannot send logs to elasticsearch with ilm policy configured

- `data_stream_ilm_policy` depends on `elasticsearch-xpack`
- elasticsearch-xpack is deprecated for elastic 8 -> should use `elasticsearch-api`
- elasticsearch-api not supported by fluent-plugin-elasticsearch yet: https://github.com/uken/fluent-plugin-elasticsearch/issues/937
- banzaicloud works around this by using elasticsearch7 and elasticsearch-xpack gems

**Workaround:** Disable `data_stream_ilm_policy`. Create it manually from kibana UI.


#### Log Format

k3s uses containerd which has a different log format from docker (json).

It uses logtag F (full log) and P (partial log that needs to be merged).

Reference:
- [containerd log format](https://github.com/fluent/fluentd-kubernetes-daemonset/issues/412)
- [containerd log format 2](https://github.com/k3s-io/k3s/issues/356)

**Solution:**

- Use the [cri parser](https://github.com/fluent/fluent-plugin-parser-cri)
- Use init container to [install fluentd plugins](https://github.com/bitnami/charts/pull/3966) to avoid using Dockerfile to build custom `bitnami/fluentd` image. This only works for fluentd plugins specifically, not for gems in general.

#### mapper_parsing_exception

> "failed to parse field [kubernetes.labels.app] of type [text] in document with id 'XUhnH4QB66Yp8NbPf7pb'. Preview of field's value: '{kubernetes={io/component=metrics}}'"

- Labels are conflicting on text and object type.
  - kubernetes.labels.app = "text"
  - kubernetes.labels.app.kubernetes.io/foo = bar -> kubernetes.labels.app = { kubernetes: { io/foo = bar } }

**Workaround:**

- [fluent-plugin-elasticsearch will not add dedot function](https://github.com/uken/fluent-plugin-elasticsearch/issues/787)
- [fluent-plugin-dedot_filter](https://github.com/lunardial/fluent-plugin-dedot_filter): kubernetes.labels.app.kubernetes.io/foo -> kubernetes.labels.app.kubernetes_io/foo = bar
  - but does not seem to work ...
- [dedot capability from kubernetes metadata filter](https://github.com/fabric8io/fluent-plugin-kubernetes_metadata_filter/issues/320)
  - need to downgrade as they have removed capability from latest version...
  - dedot [works for fluentbit](https://github.com/fluent/fluent-bit/issues/854), but no known solutions for fluentd
- Use ES index template with kubernetes.labels.app having `"type": "object"`, but this just reverse the problem for text types.
- Drop all fields `$.kubernetes.labels.app` using `record_transformer` filter, but we lose this data point.

#### Parse CRI Message

Parsing json CRI messages will result in flattened fields that will potentially lead to mapper_parsing_exception errors, just like in the case for kubernetes labels.

**Solution:**

- Use `multi_format` parser to test against multiple formats, then nest the results inside `parsed` field.
- Use `record_transformer` to perform custom ruby code to move hash in `parsed` field into `parsed.<namespace>.<container>` field.
  - But this makes it hard to normalize message field (eg. `parsed.namespace1.container1.message` and `parsed.namespace2.container2.message` cannot share the same column)

#### Limit of total fields has been exceeded while adding new fields

Update the index template settings to increase the limit from 1000 (default) to 2000.

```json
{
  "index": {
    "lifecycle": {
      "name": "k8s_policy"
    },
    "number_of_replicas": "0",
    "mapping": {
      "total_fields": {
        "limit": "2000"
      }
    }
  }
}
```

#### fluentd container logs flooded with backslashes when debugging

When fluentd logs are being read from input, it causes all the logs to be flooded with backslashes.

Disable fluentd logs when debugging.

```conf
<source>
  @type tail
  path /var/log/containers/*.log
  exclude_path /var/log/containers/*fluentd*.log
</source>
```
